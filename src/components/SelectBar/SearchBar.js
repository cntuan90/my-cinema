import React, { useState, useEffect, useCallback } from "react";
import "../SelectBar/SearchBar.css";
import Pagination from "react-js-pagination";

const SearchBar = ({ setMovieList }) => {
  const [type, setType] = useState("GP01");
  const [page, setPage] = useState(1);
  const [pageData, setPageData] = useState({});
  let keyword = "";

  const GetMovie = useCallback(async () => {
    let url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=${type}&soTrang=${page}&soPhanTuTrenTrang=10`;
    let response = await fetch(url);
    let data = await response.json();
    console.log(data);
    setPageData(data);
    setMovieList(data.items);
  }, [setMovieList, type, page]);

  // const searchByKeyword = async (keyword) => {
  //   if (keyword === '' || keyword == null) {
  //     return '';
  //   }
  //   let url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP02&tenPhim=${keyword}`;
  //   let response = await fetch(url);
  //   console.log(response)
  //   let data = await response.json();
  //   setMovieList(data.items);
  // };

  useEffect(() => {
    GetMovie();
  }, [GetMovie]);

  const handleChangeType = (selectedType) => {
    setType(selectedType);
    setPage(1);
  };

  return (
    <div>
      <div className="boxSelectBoard">
        <div id="selectBoard">
          <button
            disabled={type === "popular"}
            className="selectBoardPopularBtn"
            onClick={() => handleChangeType("GP01")}
          >
            Popular
          </button>
          <button
            disabled={type === "top_rated"}
            className=""
            onClick={() => handleChangeType("GP02")}
          >
            Top Rated
          </button>
          <button
            disabled={type === "now_playing"}
            className=""
            onClick={() => handleChangeType("GP03")}
          >
            Now Playing
          </button>
        </div>
      </div>
      <div>
        <ul>
          <div className="boxSelectBoard pageNumber">
            <Pagination
              activePage={pageData.currentPage}
              itemsCountPerPage={pageData.count}
              totalItemsCount={pageData.totalCount || 200}
              pageRangeDisplayed={pageData.totalPages}
              onChange={(pageNum) => setPage(pageNum)}
              itemClass="page-item"
              linkClass="page-link"
            />
          </div>
        </ul>
      </div>
    </div>
  );
};

export default SearchBar;
