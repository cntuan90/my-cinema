import React, { Component } from "react";
import "./style.css";

export default class NavbarHome extends Component {
  render() {
    return (
      <header className="header">
        <div className="container">
          <p className="text-right text-white mb-0">
            <i className="fa fa-phone mr-2" />
            <span className="border-right pr-3">Contact : 0330 123 4567</span>
            <i className="fa fa-search ml-2" />
          </p>
          <nav className="navbar py-0 navbar-expand-lg navbar-light">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav ml-auto border-bottom">
                <a
                  className="nav-link text-white px-4 position-relative is-active"
                  href="/"
                >
                  HOME
                </a>
                <a
                  className="nav-link text-white px-4 position-relative"
                  href="https://www.cgv.vn/default/movies/now-showing.html"
                >
                  WHAT'S ON
                </a>
                <a
                  className="nav-link text-white px-4 position-relative"
                  href="https://www.imdb.com/news/movie"
                >
                  NEWS
                </a>
                <a
                  className="nav-link text-white px-4 position-relative"
                  href="/login"
                >
                  SIGN IN
                </a>
              </div>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}
