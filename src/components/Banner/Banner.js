import React from "react";
import "./style.css";

class Banner extends React.Component {
  render() {
    return (
      <section className="slider">
        <div
          id="carouselExampleCaptions"
          className="carousel slide carousel-fade"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselExampleCaptions"
              data-slide-to={0}
              className="active"
            />
            <li data-target="#carouselExampleCaptions" data-slide-to={1} />
            <li data-target="#carouselExampleCaptions" data-slide-to={2} />
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item carousel-item1 active">
              <div className="carousel-caption d-none d-sm-block d-md-block text-left container">
                <span className="lead text-warning">
                  ACTION, ADVENTURE, FANTASY
                </span>
                <h1 className="display-4">End of the World: Part II</h1>
                <p className="lead">
                  Claritas est etiam processus dynamicus, qui sequitur
                  mutationem consuetudium lectorum. Mirum est notare quam
                  littera gothica, quam nunc putamus parum.
                </p>
              </div>
            </div>
            <div className="carousel-item carousel-item2">
              <div className="carousel-caption d-none d-md-block text-left container">
                <span className="lead text-warning">
                  ACTION, ADVENTURE, FANTASY
                </span>
                <h1 className="display-4">End of the World: Part II</h1>
                <p className="lead">
                  Claritas est etiam processus dynamicus, qui sequitur
                  mutationem consuetudium lectorum. Mirum est notare quam
                  littera gothica, quam nunc putamus parum.
                </p>
              </div>
            </div>
            <div className="carousel-item carousel-item3">
              <div className="carousel-caption d-none d-md-block text-left container">
                <span className="lead text-warning">
                  ACTION, ADVENTURE, FANTASY
                </span>
                <h1 className="display-4">End of the World: Part II</h1>
                <p className="lead">
                  Claritas est etiam processus dynamicus, qui sequitur
                  mutationem consuetudium lectorum. Mirum est notare quam
                  littera gothica, quam nunc putamus parum.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Banner;
