/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import "../MovieCard/MovieCard.css";
import { useHistory } from "react-router-dom";
import {
  PlayCircleOutlined,
  StarFilled,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Icon } from "@iconify/react";
import infoCircleOutlined from "@iconify-icons/ant-design/info-circle-outlined";

const MovieCard = ({ movie }) => {
  const history = useHistory();

  if (!movie) {
    return (
      <div style={{ textAlign: "center", margin: "30px 0" }}>
        <VideoCameraOutlined spin style={{ fontSize: "50px" }} />
      </div>
    );
  }

  return (
    <div className="flip-card">
      <div className="flip-card-inner">
        <div className="flip-card-front">
          <img src={`${movie.hinhAnh}`} className="cardImage" alt="" />
        </div>
        <div className="flip-card-back">
          <div className="movieTitle">{movie.tenPhim}</div>
          <div className="rating">
            <StarFilled style={{ color: "#db7100" }} /> &nbsp;
            <span style={{ height: "16px" }}>{movie.danhGia}</span>
          </div>
          <div className="movieDes">{movie.moTa}</div>
          <div>
            {/* <a href={history.replace(`/detail/:${movie.maPhim}`)} target="_"> */}
            <button
              onClick={() => {
                history.push(`/detail/${movie.maPhim}`);
              }}
            >
              <Icon
                style={{ fontSize: "27px", color: "black" }}
                icon={infoCircleOutlined}
              />
            </button>
            {/* </a> */}
            &nbsp;
            <a href={`${movie.trailer}`} target="_" className="movieIcon">
              <PlayCircleOutlined style={{ fontSize: "25px", color: "#000" }} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MovieCard;
