import { combineReducers } from "redux";
import detailMovieReducer from "./../../containers/HomeTemplate/DetailMoviePage/modules/reducer";
import authReducer from "./../../containers/AdminTemplate/AuthPage/modules/reducer";
import addUserReducer from "./../../containers/AdminTemplate/AddUserPage/modules/reducer";

const rootReducer = combineReducers({
  detailMovieReducer,
  authReducer,
  addUserReducer,
});

export default rootReducer;
