import HomePage from "./../containers/HomeTemplate/HomePage";
import Dashboard from "../containers/AdminTemplate/DashboardPage";
import AddUserPage from "../containers/AdminTemplate/AddUserPage";
import DetailMoviePage from "../containers/HomeTemplate/DetailMoviePage";
import EditUserPage from "../containers/AdminTemplate/EditUserPage";

const routesHome = [
  {
    exact: true,
    path: "/",
    component: HomePage,
  },
  {
    exact: false,
    path: "/detail/:id",
    component: DetailMoviePage,
  },
];

const routesAdmin = [
  {
    exact: false,
    path: "/dashboard",
    component: Dashboard,
  },
  {
    exact: false,
    path: "/add-user",
    component: AddUserPage,
  },
  {
    exact: false,
    path: "/edit-user",
    component: EditUserPage,
  },
];

export { routesHome, routesAdmin };
