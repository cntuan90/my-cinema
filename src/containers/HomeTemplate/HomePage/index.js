import React, { Component } from "react";
import Home from "./home";

export default class HomePage extends Component {
  render() {
    return (
      <>
        <Home />
      </>
    );
  }
}
