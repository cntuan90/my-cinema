import React, { useState, useEffect } from "react";
import Banner from "../../../components/Banner/Banner";
import MovieList from "../../../components/MovieList/MovieList";
import SearchBar from "../../../components/SelectBar/SearchBar";
import Header from "../../../components/NavbarHome/index";

function Home() {
  let [movieList, setMovieList] = useState([]);
  let type = "GP01";
  let [typeList, setTypeList] = useState([]);

  const getListMovie = async () => {
    let url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=${type}`;
    let response = await fetch(url);
    let data = await response.json();
    setTypeList(data.items);
  };

  useEffect(() => {
    getListMovie();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  return (
    <div>
      <Header />
      <Banner />
      <SearchBar setMovieList={setMovieList} />
      <MovieList list={movieList} typeList={typeList} />
    </div>
  );
}

export default Home;
