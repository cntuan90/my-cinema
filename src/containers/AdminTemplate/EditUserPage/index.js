import React, { useEffect, useState } from "react";
import {
  FormControl,
  Grid,
  TextField,
  InputLabel,
  Select,
  MenuItem,
  OutlinedInput,
  Button,
  Box,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { useForm, Controller } from "react-hook-form";
import Loader from "./../../../components/Loader";
import axios from "axios";

export default function EditUserPage() {
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState();
  const [res, setRes] = useState();
  const [keyword, setKeyword] = useState();
  const [listUser, setUserList] = useState([]);

  let accessToken = "";
  if (localStorage.getItem("UserAdmin")) {
    accessToken = JSON.parse(localStorage.getItem("UserAdmin")).accessToken;
  }

  const groupCodeArr = [
    { value: "GP01", text: "Group_GP01" },
    { value: "GP02", text: "Group_GP02" },
    { value: "GP03", text: "Group_GP03" },
    { value: "GP04", text: "Group_GP04" },
    { value: "GP05", text: "Group_GP05" },
  ];

  const { handleSubmit, control } = useForm({
    mode: "onChange",
    defaultValues: {
      groupCode: "GP01",
    },
  });

  const renderNoti = () => {
    if (error) {
      return <div className="alert alert-danger">{error.response.data}</div>;
    } else if (res)
      return (
        <div className="alert alert-success">
          {res.response.data || res.response}
        </div>
      );
  };

  const handleDeleteUser = (data) => {
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung",
      method: "DELETE",
      data: data,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        setRes(res);
      })
      .catch((err) => {
        setError(err);
      });
  };

  const onSearch = async (formdata) => {
    try {
      let url;
      if (keyword) {
        url = `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${formdata.groupCode}&tuKhoa=${keyword}`;
      } else {
        url = `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${formdata.groupCode}`;
      }
      let response = await fetch(url);
      let data = await response.json();
      setUserList(data);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
      setError("");
      setRes("");
    }
  };

  const fetchUser = async () => {
    try {
      setLoading(true);
      let url = `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01`;
      let response = await fetch(url);
      let data = await response.json();
      setUserList(data);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) return <Loader />;
  return (
    <Box>
      <div className="container-fluid">
        <div className="row">
          <div className="col-6 mx-auto">
            <h2 className="display-4 text-center">Quản Lý Người Dùng</h2>
          </div>
        </div>
        {renderNoti()}
        <form onSubmit={handleSubmit(onSearch)}>
          <Grid
            container={true}
            spacing={2}
            style={{ margin: "1rem 0" }}
            alignItems="center"
          >
            <Grid item={true} md={1} lg={2}>
              <Controller
                name="groupCode"
                control={control}
                render={({ ref, ...rest }) => (
                  <FormControl variant="outlined" fullWidth={true}>
                    <InputLabel shrink={true}>Mã nhóm</InputLabel>
                    <Select
                      {...rest}
                      inputRef={ref}
                      label="Mã nhóm"
                      defaultValue="GP01"
                      displayEmpty={true}
                      input={<OutlinedInput label="Mã nhóm" />}
                    >
                      {groupCodeArr.map((item, i) => (
                        <MenuItem
                          key={item.value + String(i)}
                          value={item.value}
                        >
                          {item.text}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                )}
              />
            </Grid>
            <Grid item={true} md={1} lg={2}>
              <FormControl variant="outlined" fullWidth={true}>
                <TextField
                  id="outlined-search"
                  label="TỪ KHÓA"
                  variant="outlined"
                  onChange={(e) => {
                    e.preventDefault();
                    setKeyword(e.target.value);
                  }}
                />
              </FormControl>
            </Grid>
            <Grid item={true} md={1} lg={2}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                style={{ padding: "8px 2rem", textAlign: "left" }}
              >
                Tìm người dùng
              </Button>
            </Grid>
          </Grid>
        </form>
        <table className="table">
          <thead>
            <tr>
              <th>Họ Tên</th>
              <th>Email</th>
              <th>Mã Loại User</th>
              <th>Số Điện Thoại</th>
              <th>Tài Khoản</th>
            </tr>
          </thead>
          <tbody>
            {listUser &&
              listUser.map((item, index) => {
                return (
                  <tr>
                    <td>{item.hoTen}</td>
                    <td>{item.email}</td>
                    <td>{item.maLoaiNguoiDung}</td>
                    <td>{item.soDt}</td>
                    <td>{item.taiKhoan}</td>
                    <td style={{ width: "40px", height: "40px" }}>
                      <button
                        onClick={() => {
                          handleDeleteUser(item.taiKhoan);
                        }}
                        style={{ weight: "40px", height: "40px" }}
                        class="btn btn-danger rounded-circle"
                      >
                        <DeleteIcon />
                      </button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </Box>
  );
}
